<section>
    <div class="container">
        <div class="row">
            <?php include Viewer::get_categories_path(); ?>

            <div class="col-sm-9 padding-right">
                <?php include Viewer::get_latest_products_path(); ?>
            </div>
        </div>
    </div>
</section>