<section>
    <div class="container">
        <div class="row">
            <?php include Viewer::get_categories_path(); ?>

            <div class="col-sm-9 padding-right">
               
                <?php 
                
                $latestProducts = $categoryProducts;
                include Viewer::get_latest_products_path();
                
                //Постраничная навигация
                echo $pagination->get(); 
                
                ?>

            </div>
        </div>
    </div>
</section>