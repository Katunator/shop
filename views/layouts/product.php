<div class="product-image-wrapper">
    <div class="single-products">
        <div class="productinfo text-center">
            <a href="/product/<?php echo $product['id']; ?>">
                <img src="<?php echo Product::getImage($product['id']); ?>" alt="" />
                <h3><?php echo $product['name']; ?></h3>
            </a>
            
            <h2><?php echo $product['price']." ".CURRENCY; ?></h2>

            <a href="/cart/add/<?php echo $product['id'] ?>" class="btn btn-default add-to-cart">
                <i class="fa fa-shopping-cart"></i>В корзину
            </a>
        </div>
        <?php if ($product['is_new']) : ?>
            <img src="/template/images/home/new.png" class="new" alt="" />
        <?php endif; ?>
    </div>
</div>