<div class="features_items">
    <h2 class="title text-center">Последние товары</h2>
    
    <?php foreach ($latestProducts as $product): ?>
        <div class="col-sm-4">
            <?php include Viewer::get_product_path(); ?>
        </div>
    <?php endforeach; ?>                   
</div>