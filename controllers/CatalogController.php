<?php

class CatalogController extends UserBase
{

    public function actionIndex()
    {
        $categories = Category::getCategoriesList();

        $latestProducts = Product::getLatestProducts(12);

        self::include_header();
        require_once(ROOT . '/views/catalog/index.php');
        self::include_footer();
        return true;
    }

    public function actionCategory($categoryId, $page = 1)
    {
        $categories = Category::getCategoriesList();

        $categoryProducts = Product::getProductsListByCategory($categoryId, $page);

        $total = Product::getTotalProductsInCategory($categoryId);

        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');

        self::include_header();
        require_once(ROOT . '/views/catalog/category.php');
        self::include_footer();
        return true;
    }
}
