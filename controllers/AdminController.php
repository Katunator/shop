<?php


class AdminController extends AdminBase
{

    public function actionIndex()
    {
        self::checkAdmin();

        self::include_header();
        require_once(ROOT . '/views/admin/index.php');
        self::include_footer();
        return true;
    }
}
