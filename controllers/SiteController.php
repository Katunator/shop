<?php

class SiteController extends UserBase
{

    public function actionIndex()
    {
        $categories = Category::getCategoriesList();

        $latestProducts = Product::getLatestProducts(6);

        $sliderProducts = Product::getRecommendedProducts();

        self::include_header();
        require_once(ROOT . '/views/site/index.php');
        self::include_footer();
        return true;
    }

    public function actionContact()
    {

        $userEmail = false;
        $userText = false;
        $result = false;

        if (isset($_POST['submit'])) {
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];

            $errors = false;

            if (!User::checkEmail($userEmail)) {
                $errors[] = 'Неправильный email';
            }

            if ($errors == false) {
                $adminEmail = AdminBase::getEmail();
                $message = "Текст: {$userText}. От {$userEmail}";
                $subject = 'Тема письма';
                $result = mail($adminEmail, $subject, $message);
            }
        }

        self::include_header();
        require_once(ROOT . '/views/site/contact.php');
        self::include_footer();
        return true;
    }


    public function actionAbout()
    {
        self::include_header();
        require_once(ROOT . '/views/site/about.php');
        self::include_footer();
        return true;
    }
}
