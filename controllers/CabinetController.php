<?php

class CabinetController extends UserBase
{

    public function actionIndex()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        self::include_header();
        require_once(ROOT . '/views/cabinet/index.php');
        self::include_footer();
        return true;
    }

    public function actionEdit()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $name = $user['name'];
        $password = $user['password'];

        $result = false;

        if (isset($_POST['submit'])) {

            $name = $_POST['name'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }
            if ($errors == false) {

                $result = User::edit($userId, $name, $password);
            }
        }

        self::include_header();
        require_once(ROOT . '/views/cabinet/edit.php');
        self::include_footer();
        return true;
    }
}
