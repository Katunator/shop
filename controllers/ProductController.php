<?php

class ProductController extends UserBase
{

    public function actionView($productId)
    {
        $categories = Category::getCategoriesList();

        $product = Product::getProductById($productId);

        self::include_header();
        require_once(ROOT . '/views/product/view.php');
        self::include_footer();
        return true;
    }
}
