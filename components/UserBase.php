<?php

abstract class UserBase
{
    public static function include_header()
    {
        Viewer::include_header();
    }

    public static function include_footer()
    {
        Viewer::include_footer();
    }
}