<?php

class Db
{
    public static function getConnection()
    {
        $paramsPath = ROOT . '/config/db_params.php';
        $params = include($paramsPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        $db->exec("set names utf8");

        return $db;
    }

    public static function parseQueryResult($result, ...$params)
    {
        $i = 0;
        $array = array();
        while ($row = $result->fetch()) {
            foreach ($params as $p)
            {
                $array[$i][$p] = $row[$p];
            }
            $i++;
        }
        return $array;
    }

}
