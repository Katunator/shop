<?php

class Viewer
{
    public static function include_header()
    {
        Viewer::include_layout("header");
    }
    public static function include_header_admin()
    {
        Viewer::include_layout("header_admin");
    }

    public static function include_footer()
    {
        Viewer::include_layout("footer");
    }
    public static function include_footer_admin()
    {
        Viewer::include_layout("footer_admin");
    }


    public static function get_product_path()
    {
        return Viewer::get_layout_path("product");
    }
    public static function get_latest_products_path()
    {
        return Viewer::get_layout_path("latest_products");
    }
    public static function get_categories_path()
    {
        return Viewer::get_layout_path("categories");
    }


    public static function include_layout($fileName)
    {
        include Viewer::get_layout_path($fileName);
    }

    private static function get_layout_path($fileName)
    {
        return ROOT . "/views/layouts/" . $fileName . ".php";
    }
}
