<?php

abstract class AdminBase
{
    private static function getAdminParam($param)
    {
        $paramsPath = ROOT . '/config/admin_params.php';
        $params = include($paramsPath);

        return $params[$param];
    }

    public static function getPhone()
    {
        return AdminBase::getAdminParam('phone');
    }
    
    public static function getEmail()
    {
        return AdminBase::getAdminParam('email');
    }

    public static function checkAdmin()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        if ($user['role'] == 'admin') {
            return true;
        }

        die('Access denied');
    }

    public static function include_header()
    {
        Viewer::include_header_admin();
    }

    public static function include_footer()
    {
        Viewer::include_footer_admin();
    }

}
